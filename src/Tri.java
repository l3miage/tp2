public class Tri {

    final static int N = (int) (Math.random() * (50 - 3));

    public static void main ( String[] args ){
        int [] t = creationTab();
        t = tri(t);
        for (int s : t) {
            System.out.println(s);
        }
    }

    public static int [] creationTab (){
        int [] t = new int [N];
        for (int i = 0 ; i<t.length; i++){
            t[i] = (int) (Math.random() * 100);
        }
        return t;
    }

    public static int [] tri (int [] tab){
        for (int i = 0; i <= N-2 ; i++) {
            int rangmin = i ;
            for(int j = i+1; j<= N-1; j++ ){
                if(tab[j]< tab[rangmin]){
                   rangmin=j;
                }
            }
            int aux = tab[i];
            tab[i] = tab[rangmin];
            tab[rangmin] = aux;
        }
        return tab;
    }

    public static boolean dicho (int a, int [] tab) {
        int deb = 0, fin = N - 1, milieu = (deb + fin) / 2;
        while (deb <= fin && a != tab[milieu]) {
            if (a < tab[milieu]) {
                fin = milieu - 1;
                if (a > tab[milieu]) deb = milieu + 1;
                milieu = (deb + fin) / 2;
            }

        }
        return deb <= fin;
    }
}
