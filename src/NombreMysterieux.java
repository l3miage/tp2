import java.sql.Array;
import java.util.ArrayList;

public class NombreMysterieux {

    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 10; i < 100; i++) {
            boolean[] tabBool = new boolean[10];
            boolean isOk = false;
            int j = (int) Math.pow(i, 2), k = (int) Math.pow(i, 3);
            System.out.println("Nombre occupé : " + i);

            while(j!=0) {
                int mod = j%10;
                if (!tabBool[mod])tabBool[mod] = true;
                else {
                    System.out.println(mod + " est en double");
                    isOk = true;
                }
                j-=mod;
                j/=10;
            }

            while(k!=0) {
                int mod = k%10;
                if (!tabBool[mod]) tabBool[mod] = true;
                else {
                    System.out.println(mod + " est en double");
                    isOk = true;
                }
                k-=mod;
                k/=10;
            }

            if (!isOk && tableauComplet(tabBool)) {
                System.out.println(i + " est un nombre mystérieux");
                integers.add(i);
            } else {
                System.out.println("raté...");
            }

            System.out.println("----------------------");
        }

        for (Integer i : integers) {
            System.out.print(i + ", ");
        }
    }

    private static boolean tableauComplet(boolean[] t) {
        for (boolean b : t) {
            if (!b) {
                return false;
            }
        }
        return true;
    }
}
